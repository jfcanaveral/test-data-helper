var testDataHelper = {};

// Number Utils
testDataHelper.createRandomNumericValue = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
};

// String Utils
testDataHelper.createRandomString = function(length, start, end) {
    var i;
    var output = '';
    var min = start || 'a';
    var max = end || 'z';

    for(i = 0;  i < length ; i++) {
        output += String.fromCharCode(this.createRandomNumericValue(min.charCodeAt(0), max.charCodeAt(0)));
    }

    return output;
};

testDataHelper.reverseString = function(str) {
    var strLen = str.length;
    var outputString = '';
    for (var i = 0; i < strLen; i++) {
        outputString += str.substr(strLen - 1 - i, 1);
    }

    return outputString;
};

testDataHelper.createUUID = function(){
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    });

    return uuid;
};

// Date Utils
testDataHelper.createISODate = function(date){
    var requestDate = date || new Date();
    return requestDate.toISOString().replace('Z', '+00:00');
};

testDataHelper.isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

testDataHelper.getDaysInMonth = function (year, month) {
    return [31, (this.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

testDataHelper.addDays = function(date, numOfDays, format) {
    var baseDate = date || new Date();
    var adjustments = numOfDays || 1;
    var adjusted =  baseDate.setDate(baseDate.getDate() + adjustments);

    var formatted;
    switch (format) {
        case 'string':
            formatted = new Date(adjusted).toDateString();
            break;
        case 'tz':
            formatted = new Date(adjusted).toUTCString();
            break;
        case 'sec':
            formatted = Math.floor(adjusted / 1000);
            break;
        case 'msec':
            formatted = adjusted; //milliseconds
            break;
        default:
            formatted = new Date(adjusted); //toString
    }

    return formatted;
};

testDataHelper.addMonths = function(date, numOfMonths, format) {
    var baseDate = date || new Date();
    var adjustments = numOfMonths || 1;
    var n = baseDate.getDate();
    baseDate.setDate(1);

    var adjusted =  new Date(baseDate.setMonth(baseDate.getMonth() + adjustments));
    adjusted.setDate(Math.min(n, this.getDaysInMonth(baseDate.getFullYear(), baseDate.getMonth())));

    var formatted;
    switch (format) {
        case 'string':
            formatted = new Date(adjusted).toDateString();
            break;
        case 'tz':
            formatted = new Date(adjusted).toUTCString();
            break;
        case 'sec':
            formatted = Math.floor(adjusted / 1000);
            break;
        case 'msec':
            formatted = adjusted; //milliseconds
            break;
        default:
            formatted = new Date(adjusted);
    }

    return formatted;
};

testDataHelper.addYears = function(date, numOfYears, format) {
    var adjustments = numOfYears || 1;
    return this.addMonths(date, 12 * adjustments, format);
};


module.exports = testDataHelper;
