# Welcome to `test-data-helper` node module

This is a node.js implementation of some utility functions that may ease testing, test automation, and to some extent even coding.

## Code Example

```javascript
var lib = require('test-data-helper');

var randomString = lib.createRandomString(10);
var tomorrow = lib.addDays(new Date(), 7);
```

## Available Functions

### Number Utilities
- createRandomNumericValue(min, max)

### String Utilities
- createRandomString(length, start, end)
- reverseString(string)
- createUUID()

### Date Utilities
- createISODate(date)
- isLeapYear(year)
- getDaysInMonth(year, month)
- addDays(date, numOfDays, format)
- addMonths(date, numOfMonths, format)
- addYears(date, numOfYears, format)

## License

Copyright (c) 2016, James Cañaveral <james.canaveral@gmail.com>

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.